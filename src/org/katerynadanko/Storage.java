package org.katerynadanko;

public class Storage {
    private volatile int boxes;

    public int getBoxes() {
        synchronized (Storage.class) {
            return boxes;
        }
    }

     public int increment() {
         synchronized (Storage.class) {
             return boxes++;
         }
     }
    public  int decremernt() {
        synchronized (Storage.class) {
            return boxes--;
        }
    }
}
