package org.katerynadanko;

import java.util.Date;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Car {
    Storage storage = new Storage();

    private static final CyclicBarrier barrier = new CyclicBarrier(1, () ->
            System.out.println("Переполнение ангара! Ждем!"));
    private static final Semaphore semaphore = new Semaphore(2, true);

    public static void main(String[] args) {

        Car car = new Car();

        Thread threadStorage = new Thread(() -> car.methodMakeBoxes());

        Thread thread1 = new Thread(() -> car.methodCar("Car", 1));
        Thread thread2 = new Thread(() -> car.methodCar("Car", 2));
        Thread thread3 = new Thread(() -> car.methodCar("Car", 3));

        Thread thread4 = new Thread(() -> car.methodMinibus("Minibus", 4));
        Thread thread5 = new Thread(() -> car.methodMinibus("Minibus", 5));
        Thread thread6 = new Thread(() -> car.methodMinibus("Minibus", 6));

        Thread thread7 = new Thread(() -> car.methodTruck("Truck", 7));
        Thread thread8 = new Thread(() -> car.methodTruck("Truck", 8));


        threadStorage.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        thread6.start();
        thread7.start();
        thread8.start();
    }

    public void methodCar(String name, int number) {
        Date date = new Date();
        while (true) {
            try {
                semaphore.acquire(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " " + number + " " + date +
                    " Заехал в ангар.");
            for (int i = 0; i < 2; i++) {
                condition(name, number);
            }
            System.out.println(name + " " + number + " " + date + " Выехал из ангара, " +
                    "загруженый 2 коробками. " +
                    "В ангаре осталось " + storage.getBoxes() + " коробок");
            semaphore.release(1);
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void methodMinibus(String name, int number) {
        Date date = new Date();
        while (true) {
            try {
                semaphore.acquire(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " " + number + " " + date +
                    " Заехал в ангар.");
            for (int i = 0; i < 4; i++) {
                condition(name, number);
            }
            System.out.println(name + " " + number +" " + date + " Выехал из ангара, " +
                    "загруженый 4 коробками. " +
                    "В ангаре осталось " + storage.getBoxes() + " коробок");
            semaphore.release(1);

            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void methodTruck(String name, int number) {
        Date date = new Date();
        while (true) {
            try {
                semaphore.acquire(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name + " " + number + " " + date +
                    " Заехал в ангар.");
            for (int i = 0; i < 8; i++) {
                condition(name, number);
            }

            System.out.println(name +" "+ number +" "+ date +
                    "Выехал из ангара загруженый 8 коробками. " +
                    "В ангаре осталось " + storage.getBoxes() + " коробок!");
            semaphore.release(1);
            try {
                Thread.sleep(12000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public int condition(String name, int number) {
        Date date = new Date();
        {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            storage.decremernt();
//            System.out.println(name + " " + number + " " + date +
//                    " Загрузил одну коробку. В ангаре осталось " +
//                    storage.getBoxes() + " коробок");
        }
            return storage.getBoxes();
    }


    public void methodMakeBoxes() {
        while (true) {
            while (storage.getBoxes() < 30) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < 5; i++) {
                    storage.increment();
                }
                System.out.println("Добавилось 5 коробок. Всего в ангаре " +
                        storage.getBoxes() + " коробок");


                if (storage.getBoxes() >= 30) {
                    try {
                        barrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }



}
